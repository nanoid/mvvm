package com.example.inversebinding

import android.databinding.BindingAdapter
import android.databinding.InverseBindingAdapter
import android.databinding.InverseBindingListener
import android.support.constraint.ConstraintLayout
import android.support.v4.view.ViewPager
import android.support.v4.widget.SwipeRefreshLayout
import android.util.Log
import com.example.inversebinding.ui.OnBoardingActivity


@BindingAdapter("backgroundRes")
fun ConstraintLayout.setBackgroundRes(res: Int) {
    setBackgroundResource(res)
}

@InverseBindingAdapter(attribute = "changePage")
fun ViewPager.getChangePage(): Int = currentItem


@BindingAdapter("changePage")
fun ViewPager.setChangePage(position: Int) {
    currentItem = position
}


@BindingAdapter("changePageAttrChanged")
fun ViewPager.setChangePage(listener: InverseBindingListener) {



    addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(p0: Int) {
        }

        override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            listener.onChange()
        }

        override fun onPageSelected(p0: Int) {
        }
    })
}

