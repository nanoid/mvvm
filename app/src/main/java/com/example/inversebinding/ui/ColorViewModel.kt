package com.example.inversebinding.ui

import android.arch.lifecycle.ViewModel
import com.example.inversebinding.R

abstract  class ColorViewModel:ViewModel(){
    open val colorRes = R.color.colorAccent
}
