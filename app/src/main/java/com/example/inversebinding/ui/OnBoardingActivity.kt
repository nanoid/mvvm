package com.example.inversebinding.ui

import android.arch.lifecycle.ViewModel
import android.databinding.DataBindingUtil
import android.databinding.ObservableInt
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.PagerAdapter
import android.support.v7.app.AppCompatActivity
import com.example.inversebinding.R
import com.example.inversebinding.databinding.ActivityBoardingBinding


class OnBoardingActivity : AppCompatActivity() {

    lateinit var binding: ActivityBoardingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_boarding)
        binding.viewModel = OnBoardingViewModel()
    }

    private inner class PagerAdapterImpl : FragmentPagerAdapter(supportFragmentManager) {

        private val pages = enumValues<Page>()

        override fun getCount() = pages.size
        override fun getItem(position: Int) = pages[position].fragment()
    }


    inner class OnBoardingViewModel : ViewModel() {
        var position = ObservableInt(0)
        val pagerAdapter: PagerAdapter = PagerAdapterImpl()

        fun onBackPresses() {
           when(position.get()){

           }
            position.get().takeIf{ (it!=0)}?.let { position.set(it - 1)} ?:finish()
        }
    }


    enum class Page(val fragment: () -> Fragment) {
        RED({ ColorFragment().also {  } }),
        GREEN({ ColorFragment() }),
        BLUE({ ColorFragment() })
    }

    override fun onBackPressed() {
        binding.viewModel!!.onBackPresses()
    }
}
